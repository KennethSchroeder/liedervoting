package logic;

import java.util.List;

import database.DatabaseConnect;


public class MusicvotingLogik {

	private DatabaseConnect dbconn;
	private String currentUser;
	private String messageTextLogIn;

	public MusicvotingLogik() {
		this.dbconn = new DatabaseConnect();
		this.currentUser = null;
		this.messageTextLogIn = "No current User";
	}

	public String getMessageTextLogIn() {
		return messageTextLogIn;
	}

	public String getCurrentUser() {
		return currentUser;
	}

	public boolean registerUser(String username) {
		return dbconn.userEintragen(username);
	}

	public boolean login(String username) {
		if (userExists(username)) {
			this.currentUser = username;
			this.messageTextLogIn = "LogIn erfolgreich";
			return true;
		} else {
			this.messageTextLogIn = "User existiert nicht!";
			return false;
		}
	}

	public void logout(String username) {
		this.currentUser = null;
		this.messageTextLogIn = "ausgeloggen erfolgreich";
	}

	public boolean userExists(String username) {
		return dbconn.userExists(username);
	}

	/**
	 * tr�gt einen vollst�ndig ausgef�llten Vorschlag in die Datenbank ein
	 * @param m - Musiktitelobjekt
	 */
	public void musiktitelEintragen(Musiktitel m) {
		if(m.getBandname() != "" && m.getTitelname() != "" && m.getGenre() != "")
			dbconn.musiktitelEintragen(m);		
	}

	public String[][] getMusiktitel() {
		List<Musiktitel> liste = dbconn.getMusiktitel();
		String[][] musikarray = new String[liste.size()][3];
		int index = 0;
		for(Musiktitel m: liste){
			musikarray[index][0] = m.getBandname();
			musikarray[index][1] = m.getTitelname();
			musikarray[index][2] = m.getGenre();
			index++;
		}
		return musikarray;
	}

	public void voteEintragen(String loginname, Musiktitel m) {
		if(!userExists(loginname))
			registerUser(loginname);

		dbconn.voteEintragen(loginname, m);
	}

	public String[][] getPlaylist() {	
		return dbconn.getPlaylist();
	}

}

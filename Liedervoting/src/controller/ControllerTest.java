package controller;

import database.*;

public class ControllerTest {

	public static void main(String[] args) {
		Controller ctrl = new Controller();
		Gast g = new Gast("Tom", "Schrogl");
		Lied l = new Lied("Atemlos", "Helene Fischer", "Schlager");

		ctrl.gast_eintragen(g);
		ctrl.lied_eintragen(l);
		
		ctrl.gast_eintragen(g);
		ctrl.lied_eintragen(l);
		
		ctrl.voten(g, l);
		
	}

}

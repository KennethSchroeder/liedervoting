package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import database.DatabaseConnect;
import database.Gast;
import database.Lied;
import controller.Controller;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.awt.event.ActionEvent;

public class MusikVoten extends JFrame {

	private JPanel contentPane;
	private JTextField text_Name;
	private Gast gast;
	private String loginname;
	private DatabaseConnect db;
	private Controller ctrl;
	private Lied lied;
	LinkedList lieder_list1 = new LinkedList();
	private String[] spaltennamen = {"Bandname", "Titelname", "Genre"};

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MusikVoten frame = new MusikVoten();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public String[][] tabledatb() {
		String[][] tabledata = new String[lieder_list1.size()][spaltennamen.length];
		for (int i = 0; i < lieder_list1.size(); i++) {
			//tabledata[i][0] = lieder_list1.get(i).toString();
			tabledata[i][1] = lieder_list1.get(i).toString();
			tabledata[i][2] = lieder_list1.get(i).toString();
			tabledata[i][3] = lieder_list1.get(i).toString();
		}
		return tabledata;
	}
	
	/**
	 * Create the frame.
	 */
	public MusikVoten() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_Voten = new JPanel();
		contentPane.add(panel_Voten, BorderLayout.EAST);
		
		JButton btn_Voten = new JButton("Voten");
		btn_Voten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(text_Name.getText().equals(""))
					JOptionPane.showMessageDialog(null, "Name darf nicht leer sein!",
							"ERROR",
						    JOptionPane.ERROR_MESSAGE);
				else{
					text_Name.setBackground(Color.WHITE);
					loginname = text_Name.getText();
					JTable panel_Lieder = null;
					int index = panel_Lieder.getSelectedRow();
				
				}
			}
		});
		panel_Voten.add(btn_Voten);
		
		
		
		JPanel panel_North = new JPanel();
		contentPane.add(panel_North, BorderLayout.NORTH);
		panel_North.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lbl_Username = new JLabel("Name");
		panel_North.add(lbl_Username);
		
		text_Name = new JTextField();
		panel_North.add(text_Name);
		text_Name.setColumns(10);
					
		
		// Die Daten f�r die JTable
	      Object[][] data = new Object[lieder_list1.size()][spaltennamen.length];
	      //Daten ende
		
		
		
		
		
		JTable panel_Lieder = new JTable(data, spaltennamen);
		panel_Lieder.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		
		JScrollPane scrollPane = new JScrollPane(panel_Lieder);
		panel_Lieder.setFillsViewportHeight(true);
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		
	}

}

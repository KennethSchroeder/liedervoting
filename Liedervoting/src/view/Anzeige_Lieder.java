package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import controller.Controller;
import database.Gast;
import database.Lied;
import database.DatabaseConnect;

public class Anzeige_Lieder extends JFrame {

	private JPanel contentPane;
	private DatabaseConnect db;
	private Controller ctrl;
	private Gast gast;
	private Lied lied;
	private String name;
	private String vorname;
	private String interpret;
	private String titele;
	private String genre;
	private String[] spaltennamen = {"Anzahl Votes", "Bandname", "Titelname", "Genre"};
	public Lied getLied() {
		return lied;
	}



	public void setLied(Lied lied) {
		this.lied = lied;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getInterpret() {
		return interpret;
	}



	public void setInterpret(String interpret) {
		this.interpret = interpret;
	}



	public String getTitele() {
		return titele;
	}



	public void setTitele(String titele) {
		this.titele = titele;
	}



	public String getGenre() {
		return genre;
	}



	public void setGenre(String genre) {
		this.genre = genre;
	}



	LinkedList lieder_list1 = new LinkedList();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Anzeige_Lieder frame = new Anzeige_Lieder();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	
	public String[][] tabledatb() {
		String[][] tabledata = new String[lieder_list1.size()][spaltennamen.length];
		for (int i = 0; i < lieder_list1.size(); i++) {
			//tabledata[i][0] = lieder_list1.get(i).toString();
			tabledata[i][1] = lieder_list1.get(i).toString();
			tabledata[i][2] = lieder_list1.get(i).toString();
			tabledata[i][3] = lieder_list1.get(i).toString();
		}
		return tabledata;
	}
	
	
	
	/**
	 * Create the frame.
	 */
	public Anzeige_Lieder() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		
		// Die Daten f�r die JTable
	      Object[][] data = new Object[lieder_list1.size()][spaltennamen.length];//Daten ende
	      
	     
		JTable table = new JTable( data, spaltennamen);
		JScrollPane panel_Lieder = new JScrollPane(table);
		contentPane.add(panel_Lieder, BorderLayout.CENTER);	
	}



	public String getVorname() {
		return vorname;
	}



	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

}

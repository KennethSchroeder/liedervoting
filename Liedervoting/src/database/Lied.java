package database;

public class Lied {

private String titel;
private String inpterpret;
private String genre;

public Lied(String t, String i, String g) {
	this.titel = t;
	this.inpterpret = i;
	this.genre = g;
}


public String getTitel() {
	return titel;
}

public void setTitel(String titel) {
	this.titel = titel;
}

public String getInpterpret() {
	return inpterpret;
}

public void setInpterpret(String inpterpret) {
	this.inpterpret = inpterpret;
}

public String getGenre() {
	return genre;
}

public void setGenre(String genre) {
	this.genre = genre;
}


	
}

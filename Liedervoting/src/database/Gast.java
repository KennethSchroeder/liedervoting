package database;

public class Gast {

private String vorname;
private String nachname;

public Gast(String v, String n) {
	this.vorname = v;
	this.nachname = n;
}

public String getVorname() {
	return vorname;
}

public void setVorname(String vorname) {
	this.vorname = vorname;
}

public String getNachname() {
	return nachname;
}

public void setNachname(String nachname) {
	this.nachname = nachname;
}
	

}

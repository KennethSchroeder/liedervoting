/*First set of examples */
INSERT INTO t_gaeste (Vorname, Nachname) VALUES
	("Tom", "Schrogl");
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Queen", "Don't Stop Me Now", "Rock");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(1,1);
	DO SLEEP(1);

INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("OOMPH!", "Augen auf!", "Rock");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(1,2);
	DO SLEEP(1);

INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Fettes Brot", "Emanuela", "Rock");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(1,3);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Fettes Brot", "Schwule Mädchen", "Rock");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(1,4);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Leider Geil(Leider geil)", "Deichkind", "Rock");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(1,5);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Haus am See", "Peter Fox", "Rock");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(1,6);	
	DO SLEEP(1);
	

/*Second set of examples */
INSERT INTO t_gaeste (Vorname, Nachname) VALUES
	("Kenneth", "Schroeder");
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("EvilGinger", "Burn'em all", "rooDevil");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(2,7);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Alles neu", "Peter Fox", "rooDevil");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(2,8);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("The Beatiful People", "Marilyn Manson", "rooDevil");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(2,9);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Sweet Dreams", "Eurythmics", "rooDevil");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(2,10);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Schlechtes Vorbild", "Sido", "rooDevil");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(2,11);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Hold the Line", "Toto", "rooDevil");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(2,12);
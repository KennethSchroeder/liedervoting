CREATE TABLE T_Gaeste
(
   P_G_ID INTEGER AUTO_INCREMENT,
	Vorname VARCHAR(15) NOT NULL,
	Nachname VARCHAR(15) NOT NULL,
   PRIMARY KEY(P_G_ID)
);

CREATE TABLE T_Lieder
(
	P_L_ID INTEGER AUTO_INCREMENT,
	Bandname VARCHAR(30) NOT NULL,
	Titelname VARCHAR(30) NOT NULL,
	Genre VARCHAR(30) NOT NULL,
   PRIMARY KEY(P_L_ID)
);

CREATE TABLE T_Gaeste_Lieder
(
	P_V_ID INTEGER AUTO_INCREMENT,
	PF_G_ID INTEGER NOT NULL,
	PF_L_ID INTEGER NOT NULL,
	Zeitstempel TIMESTAMP NOT NULL,
	PRIMARY KEY(P_V_ID,PF_G_ID,PF_L_ID),
	FOREIGN KEY (PF_G_ID)
           REFERENCES T_Gaeste(P_G_ID) ON UPDATE CASCADE
			  						    ON DELETE CASCADE,
	FOREIGN KEY (PF_L_ID)
           REFERENCES T_Lieder(P_L_ID) ON UPDATE CASCADE
			  							ON DELETE CASCADE
);


ALTER TABLE t_gaeste
ADD Hausname varchar(125);

/*First set of examples */
INSERT INTO t_gaeste (Vorname, Nachname) VALUES
	("Tom", "Schrogl");
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Queen", "Don't Stop Me Now", "Rock");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(1,1);
	DO SLEEP(1);

INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("OOMPH!", "Augen auf!", "Rock");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(1,2);
	DO SLEEP(1);

INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Fettes Brot", "Emanuela", "Rock");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(1,3);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Fettes Brot", "Schwule M�dchen", "Rock");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(1,4);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Deichkind", "Leider Geil(Leider geil)", "Rock");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(1,5);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Peter Fox", "Haus am See", "Rock");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(1,6);	
	DO SLEEP(1);
	

/*Second set of examples */
INSERT INTO t_gaeste (Vorname, Nachname) VALUES
	("Kenneth", "Schroeder");
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("EvilGinger", "Burn'em all", "rooDevil");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(2,7);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Peter Fox", "Alles neu", "rooDevil");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(2,8);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Marilyn Manson", "The Beatiful People", "rooDevil");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(2,9);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Eurythmics", "Sweet Dreams", "rooDevil");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(2,10);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Sido", "Schlechtes Vorbild", "rooDevil");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(2,11);
	DO SLEEP(1);
	
INSERT INTO t_lieder (Bandname, Titelname, Genre) VALUES
	("Toto", "Hold the Line", "rooDevil");
	
INSERT INTO t_gaeste_lieder (PF_G_ID, PF_L_ID) VALUES
	(2,12);

/*Ausgabe der letzten 5 Votes*/
SELECT PF_G_ID, t_lieder.Bandname, t_lieder.Titelname FROM t_gaeste_lieder AS t_votes
LEFT JOIN t_lieder
ON t_votes.PF_L_ID = t_lieder.P_L_ID
WHERE PF_G_ID = 1 ORDER BY Zeitstempel DESC LIMIT 5;